# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import unicode_literals

from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelSQL
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition


class Plan(metaclass=PoolMeta):
    __name__ = 'commission.plan'
    percentage = fields.Float('Percentage', digits=(2,2))
    invoice_commission_sequence = fields.Many2One('ir.sequence',
        'Invoice Commission Sequence')


class Commission(metaclass=PoolMeta):
    __name__ = 'commission'
    origin_state = fields.Function(fields.Selection([
            ('', ''),
            ('posted', 'Posted'),
            ('paid', 'Paid'),
            ('cancel', 'Canceled'),
        ], 'Origin State'), 'get_origin_state')

    def get_origin_state(self, name):
        state = ''
        Invoice = Pool().get('account.invoice')
        if self.origin and self.origin.__name__ == 'account.invoice':
            invoice = Invoice(self.origin.id)
            if invoice and invoice.state in ['posted', 'paid', 'cancel']:
                state = invoice.state
        return state

    # def get_invoice_state(self, name):
    #     pool = Pool()
    #     Sale = pool.get('sale.sale')
    #     Invoice = pool.get('account.invoice')
    #     state = super(Commission, self).get_invoice_state(name)
    #     if not if self.invoice_line and self.origin and self.origin.__name__ == 'account.invoice':
    #         invoice = Invoice(self.origin.id)
    #         for line in invoice.lines:
    #             if line.origin and line.origin.__name__ == 'sale.sale':
    #                 sale = Sale(self.origin.id)
    #                 journals = [line.journal for line in sale.payments if line.journal.kind == 'commission']
    #                 if journals:
    #                     state = 'paid'
    #     return state

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        origins = super(Commission, cls)._get_origin()
        origins.append('account.invoice')
        return origins

    @classmethod
    def set_number_invoice(self, commission):
        # Sequence = Pool().get('ir.sequence')
        if commission.agent.plan.invoice_commission_sequence:
            number = commission.agent.plan.invoice_commission_sequence.get()
            commission.invoice_line.invoice.write([commission.invoice_line.invoice],
                {'number': number, 'equivalent_invoice': True})


class CreateInvoiceAsk(metaclass=PoolMeta):
    __name__ = 'commission.create_invoice.ask'
    agent = fields.Many2One('commission.agent', 'Agent')


class CreateInvoice(metaclass=PoolMeta):
    __name__ = 'commission.create_invoice'

    def get_domain(self):
        domain = super(CreateInvoice, self).get_domain()
        if self.ask.agent:
            domain.append(('agent', '=', self.ask.agent.id))
        return domain


class CreateInvoiceDirect(Wizard):
    'Create Invoice Direct'
    __name__ = 'commission.create_invoice_direct'
    start_state = 'create_invoice'
    create_invoice = StateTransition()

    def transition_create_invoice(self):
        pool = Pool()
        Commission = pool.get('commission')
        ids = Transaction().context['active_ids']
        commissions = Commission.browse(ids)
        Commission.invoice(commissions)
        return 'end'
