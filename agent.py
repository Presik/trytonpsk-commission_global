# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import unicode_literals
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelSQL, ModelView
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateReport, Button, StateTransition
from trytond.report import Report


class AgentCommissionSalesStart(ModelView):
    'Agent Commission Sales Start'
    __name__ = 'commission_global.agent_commission_sales.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company',
            required=True)
    agent = fields.Many2One('commission.agent', 'Agent')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class AgentCommissionSales(Wizard):
    'Agent Commission Sales'
    __name__ = 'commission_global.agent_commission_sales'
    start = StateView('commission_global.agent_commission_sales.start',
        'commission_global.agent_commission_sales_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('commission_global.agent_commission_sales_report')

    def do_print_(self, action):
        agent_id = None
        if self.start.agent:
            agent_id = self.start.agent.id
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'agent': agent_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AgentCommissionSalesReport(Report):
    __name__ = 'commission_global.agent_commission_sales_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Agent = pool.get('commission.agent')
        PriceList = pool.get('product.price_list')
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        dom = [
            ('company', '=', data['company']),
            ('state', 'in', ['posted', 'paid']),
            ('invoice_date', '>=', data['start_date']),
            ('invoice_date', '<=', data['end_date']),
        ]
        if data['agent']:
            dom.append(
                ('agent', '=', data['agent']),
            )

        invoices = Invoice.search([dom])
        agents = {}
        total_commission = []

        for invoice in invoices:
            if not invoice.agent or invoice.amount_to_pay != Decimal('0.0'):
                continue
            agent_id = invoice.agent.id
            commission_ = 0

            for l in invoice.lines:
                if hasattr(l, 'commission_amount') and l.commission_amount:
                    commission_ += (l.commission_amount)
                else:
                    if l.unit_price and l.product and l.quantity and l.origin \
                        and l.origin.sale.price_list:
                        percent_commission = l.origin.sale.price_list.percent_commission
                        if percent_commission:
                            commission_ += round((((l.unit_price) * int(l.quantity)) * percent_commission), 0)
            if commission_ == 0:
                continue
            if invoice.total_amount < 0 and commission_ > 0:
                commission_ = commission_ * (-1)

            commission_ = round(commission_, 0)
            total_commission.append(commission_)

            if agent_id not in agents.keys():
                agents[agent_id] = {
                    'agent': invoice.agent.party.name,
                    'invoices':[],
                }
            agents[agent_id]['invoices'].append({
                'number': invoice.number + ' [ ' + invoice.lines[0].origin.sale.number + ' ]',
                'date': invoice.invoice_date,
                'commission': commission_,
                'party': invoice.party.name,
            })

        report_context['records'] = agents.values()
        report_context['total_commission'] = sum(total_commission)
        report_context['start_date'] = data['start_date']
        agent_ = ''
        if data['agent']:
            agent_ = Agent(data['agent']).party.name
        report_context['agent'] = agent_
        report_context['end_date'] = data['end_date']
        report_context['company'] = Company(data['company']).rec_name
        return report_context
