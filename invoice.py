# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    commission = fields.Float('Commission', states={
        'readonly': True
    })

    @classmethod
    def create_commissions(cls, invoices):
        commissions = []
        for invoice in invoices:
            if invoice.agent and invoice.agent.plan and invoice.agent.plan.percentage:
                commissions.extend(invoice.create_commission_by_percentage())
            else:
                _comms = super(Invoice, cls).create_commissions([invoice])
                if _comms:
                    commissions.extend(_comms)

        return commissions

    def create_commission_by_percentage(self):
        pool = Pool()
        res = []
        Commission = pool.get('commission')
        percentage = self.commission
        amount = self.untaxed_amount * Decimal(percentage / 100.0)
        digits = Commission.amount.digits
        exp = Decimal(str(10.0 ** -digits[1]))
        if amount != 0:
            record = {
                'agent': self.agent.id,
                'product': self.agent.plan.commission_product,
                'amount': amount.quantize(exp),
                'date': self.invoice_date,
                'origin': str(self),
            }
            res = Commission.create([record])
        return res
