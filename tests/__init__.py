# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
try:
    from trytond.modules.commission_global.tests.test_commission_global import suite
except ImportError:
    from .test_commission_global import suite

__all__ = ['suite']
